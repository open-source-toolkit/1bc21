# 前端人脸识别资源文件

## 描述
本仓库提供了web端及移动端H5前端人脸识别所必须的`tracking-min.js`与`face-min.js`文件。这些文件是实现前端人脸识别功能的关键资源。

## 使用方法
1. 在您的Vue项目中，新建一个名为`assets`的文件夹。
2. 将本仓库中的`tracking-min.js`与`face-min.js`文件解压到新建的`assets`文件夹内。
3. 在您的`.vue`文件中，通过`import`或`script`标签引用这两个文件。

## 示例
```javascript
// 在.vue文件中引用
import 'assets/tracking-min.js';
import 'assets/face-min.js';
```

## 注意事项
- 确保文件路径正确，以便能够正确加载资源文件。
- 如果您在项目中遇到任何问题，请检查文件路径或参考相关文档进行调试。

## 贡献
如果您有任何改进建议或发现了问题，欢迎提交Issue或Pull Request。

## 许可证
本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。